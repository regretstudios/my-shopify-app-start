import "reflect-metadata";
import "dotenv/config";
import "isomorphic-fetch";

import { startServerShopify } from "./startServerShopify";

startServerShopify();

export { startServerShopify };

// start the server on the given port
