import * as Koa from "koa";
import * as Session from "koa-session";
import * as Views from "koa-views";
import * as Logger from "koa-logger";
import * as Cors from "@koa/cors";
import { pathView } from "./pathView";

const settingKoaApp = (app: Koa, SHOPIFY_SECRET: string) => {
  app.keys = [SHOPIFY_SECRET];
  app.use(Logger());
  app.use(Cors());
  app.use(Session(app));
  app.use(
    Views(pathView, {
      extension: "ejs"
    })
  );
};

export { settingKoaApp };
