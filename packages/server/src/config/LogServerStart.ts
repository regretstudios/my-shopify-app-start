import { ApolloServer } from "apollo-server-koa";
export function LogServerStart(PORT: string | number, server: ApolloServer) {
  console.log(
    `🚀 Server ready at http://localhost:${PORT}${server.graphqlPath}`
  );
  console.log(
    `🚀 Server shopify listening on port http://localhost:${PORT}/api/shopify/graphql`
  );
}
