import * as path from "path";

export const pathView = path.join(__dirname, "../views");
export const PROXY_BASE_PATH = "/graphql";
export const GRAPHQL_PATH = "/admin/api/graphql.json";
