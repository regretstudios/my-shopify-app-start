import { IResolvers } from "graphql-tools";
import { User } from "../../../../entity/User";

export const resolvers: IResolvers = {
  Query: {
    me: (_, __, { ctx }) => {
      if (!ctx.session.userId) {
        throw new Error("Not Found");
      }

      return User.findOne(ctx.session.userId);
    }
  }
};
