import { IResolvers } from "graphql-tools";

import { User } from "../../../../entity/User";
import { validateUserPassword } from "./validateUserPassword";
import { validateUserInput } from "./validateUserInput";

export const resolvers: IResolvers = {
  Mutation: {
    login: async (_, { email, password }, { ctx }) => {
      const userInput = new User();
      userInput.email = email;
      userInput.password = password;

      await validateUserInput(userInput);

      const user = await User.findOne({ where: { email } });
      if (!user) {
        throw new Error("Not found email");
      }

      await validateUserPassword(user, password);

      ctx.session.userId = user.id;

      return user;
    }
  }
};
