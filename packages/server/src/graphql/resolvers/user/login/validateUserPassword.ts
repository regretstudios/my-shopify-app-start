import { verify } from "argon2";
import { User } from "../../../../entity/User";

export async function validateUserPassword(user: User, password: any) {
  const valid = await verify(user.password, password);

  if (!valid) {
    throw new Error("Error Password");
  }
}
