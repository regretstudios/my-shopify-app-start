import { isEmpty } from "ramda";
import { User } from "../../../../entity/User";
import {user as userValidate} from "@key46/validate";

export async function validateUserInput(userInput: User) {
  const errors = await userValidate.validate(userInput);

  if (isEmpty(errors)) {
    throw new Error(JSON.stringify(errors));
  }
}
