import { IResolvers } from "graphql-tools";
import { isEmpty } from "ramda";
import { hash } from "argon2";

import { User } from "../../../../entity/User";
import { user as userValidate } from "../../../../../../validate/src/user";

export const resolvers: IResolvers = {
  Mutation: {
    register: async (_, { email, password }) => {
      const userInput = new User();
      userInput.email = email;
      userInput.password = password;
      const errors = await userValidate.validate(userInput);

      if (isEmpty(errors)) {
        throw new Error(JSON.stringify(errors));
      }

      const hashedPassword = await hash(password);

      await User.create({
        email,
        password: hashedPassword
      }).save();

      return true;
    }
  }
};
