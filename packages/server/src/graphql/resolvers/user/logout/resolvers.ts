import { IResolvers } from "graphql-tools";

export const resolvers: IResolvers = {
  Mutation: {
    logout: async (_, __, { ctx, res }) => {
      await new Promise(resPromise => ctx.session.destroy(() => resPromise()));
      res.clearCookie("connect.sid");
      return true;
    }
  }
};
