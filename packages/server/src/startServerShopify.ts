import * as Koa from "koa";
import * as Mount from "koa-mount";

import { ApolloServer } from "apollo-server-koa";
import createShopifyAuth, { verifyRequest } from "@shopify/koa-shopify-auth";
import { createConnection } from "typeorm";

import { homeRouter } from "./routers";
import { genSchema, KoaGraphqlProShopify } from "./utils";
import { settingKoaApp, LogServerStart } from "./config";

export const startServerShopify = async () => {
  // Connect to server postgres using typeorm
  await createConnection();

  // Get shopify key and secret and check
  const {
    SHOPIFY_API_KEY = null,
    SHOPIFY_SECRET = null,
    PORT = 5000
  } = process.env;
  if (!SHOPIFY_SECRET || !SHOPIFY_API_KEY) {
    throw new Error(
      "Please set SHOPIFY_SECRET and SHOPIFY_API_KEY in .env file"
    );
  }

  // Create new ApolloServer(apollo-server-koa) with schema by genSchema and context is koa ctx
  const server = new ApolloServer({
    schema: genSchema(),
    context: ({ ctx }: { ctx: Koa.Context }) => ({ ctx })
  });

  // Create new Koa app and setting it like session, cors, ... see more in ./config/settingKoaApp
  const app = new Koa();
  settingKoaApp(app, SHOPIFY_SECRET);

  // Add middleware of ApolloServer for servers graphql with router /api/graphql and cors is localhost:3000
  server.applyMiddleware({
    app,
    path: "/api/graphql",
    cors: {
      credentials: true,
      origin: "http://localhost:3000"
    }
  });

  // Create new routes Home
  app.use(homeRouter.routes());

  // Use middleware createShopifyAuth to create new auth middleware for app
  app.use(
    createShopifyAuth({
      apiKey: SHOPIFY_API_KEY,
      secret: SHOPIFY_SECRET,
      scopes: ["write_products"],
      afterAuth(ctx) {
        const { shop = null, accessToken = null }: any = ctx.session;
        console.log("We did it!", shop, accessToken);
        if (ctx.session && ctx.session.shop && ctx.session.accessToken) {
          ctx.session.shop = shop;
          ctx.session.accessToken = accessToken;
        }

        ctx.redirect("/");
      }
    })
  );

  // Config verify request setting for createShopifyAuth and all router under this
  app.use(
    verifyRequest({
      authRoute: "/auth",
      fallbackRoute: "/install"
    })
  );

  // Create new routes /api/test/graphql for servers graphql proxy to shopify
  app.use(Mount("/api/test", KoaGraphqlProShopify));

  // Start Server and get callback
  app.listen(PORT, () => {
    LogServerStart(PORT, server);
  });
};
