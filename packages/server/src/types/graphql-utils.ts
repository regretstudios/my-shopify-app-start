import { Context } from "koa";
import { Session } from "koa-session";

export interface Session extends Session {
  userId?: string;
  shop?: string;
  accessToken?: string;
}

export interface Context {
  url: string;
  session: Session;
  ctx: Context;
}

export type Resolver = (
  parent: any,
  args: any,
  context: Context,
  info: any
) => any;

export interface ResolverMap {
  [key: string]: {
    [key: string]: Resolver | { [key: string]: Resolver };
  };
}
