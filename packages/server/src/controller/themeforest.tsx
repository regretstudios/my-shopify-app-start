function verifyPurchase(req, res, next) {
    const username = dataJson.tf.username;
    const api_key = dataJson.tf.api_key;
    const url = 'http://marketplace.envato.com/api/edge/' + username + '/' + api_key + '/verify-purchase:' + req.query.code + '.json';
    request(
        {
            url: url,
            method: 'GET',
            headers: {
                'User-Agent': 'App Server'
            }
        },
        function(err, resp, data) {
            const body = JSON.parse(data);
            const verifyItem = body['verify-purchase'];
            res.locals = verifyItem;
            next();
        }
    );
}
// Main function to get theme info from database
function getThemeInfo(req, res, next) {
    // Get from DB first
    const themeId = req.query.id;
    const selectQuery = {
        text: 'SELECT * FROM tbl_themestores WHERE id = $1',
        values: [themeId]
    };
    (async () => {
        const client = await pool.connect();
        try {
            const result = await client.query(`SELECT * FROM tbl_themestores WHERE id ='${themeId}'`);
            if (result.rowCount > 0) {
                res.locals = result.rows[0];
                next();
            } else {
                res.send('Server has no info about this theme!!!');
                client.release();
                return;
            }
            next();
        } finally {
            client.release();
        }
    })().catch(e => console.log(e));
    // pool.connect(function(err, client, release) {
    //     if (err) {
    //         return console.error('Error acquiring client', err.stack);
    //     }
    //     client.query(selectQuery, function(err, data) {
    //         if (!data.rows[0]) {
    //             release();
    //             res.send('Server has no info about this theme!!!')
    //             return;
    //         }
    //         res.locals = data.rows[0];
    //         next();
    //     })
    // });
}
