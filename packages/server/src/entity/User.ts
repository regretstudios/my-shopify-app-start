import { Entity, PrimaryGeneratedColumn, Column, BaseEntity } from "typeorm";
import { IsEmail } from "class-validator";

@Entity("users")
export class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column("text", { unique: true })
  @IsEmail()
  email: string;

  @Column("text", { nullable: true })
  purchaseCode: string | null;

  @Column("text", { default: "free-trial" })
  type: string;

  // @Column("text", { nullable: true })
  // ccLast4: string | null;

  @Column({ type: "text", array: true, nullable: true })
  shopifyStore: string[];

  @Column("text")
  password: string;
}
