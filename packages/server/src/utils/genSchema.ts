import { fileLoader, mergeTypes, mergeResolvers } from "merge-graphql-schemas";
import * as path from "path";
import { writeFileSync, readFileSync } from "fs";
import { makeExecutableSchema } from "graphql-tools";
import * as glob from "glob";

export const genSchema = () => {
  const pathToModules = path.join(__dirname, "../graphql");
  const graphqlTypes = glob
    .sync(`${pathToModules}/**/*.?(gql|graphql)`)
    .map(x => readFileSync(x, { encoding: "utf8" }));

  const graphqlResolvers = glob
    .sync(`${pathToModules}/**/resolvers.?s`)
    .map(resolver => require(resolver).resolvers);

  const typeDefs = mergeTypes(
    fileLoader(`${pathToModules}/**/*.?(gql|graphql)`),
    { all: true }
  );
  writeFileSync("schema.graphql", typeDefs);

  return makeExecutableSchema({
    typeDefs: mergeTypes(graphqlTypes, { all: true }),
    resolvers: mergeResolvers(graphqlResolvers)
  });
};

// const graphqlFile = glob
//   .sync(`${pathToModules}/**/*.?s`)
//   .map(x => readFileSync(x, { encoding: "utf8" }));
// writeFileSync("joinedResolvers.ts", graphqlFile);
