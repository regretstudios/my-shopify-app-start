import * as KoaBetterHttpProxy from "koa-better-http-proxy";

import { PROXY_BASE_PATH, GRAPHQL_PATH } from "../config/pathView";

export const KoaGraphqlProShopify = async (ctx: any, next: any) => {
  ctx.body = ctx.session;
  const { session: { shop = null, accessToken = null } = {} } = ctx;
  if (accessToken === null || shop === null) {
    ctx.throw(403, "Unauthorized");
    return;
  }
  if (ctx.path !== PROXY_BASE_PATH || ctx.method !== "POST") {
    await next();
    return;
  }
  await KoaBetterHttpProxy(shop, {
    https: true,
    parseReqBody: false,
    headers: {
      "Content-Type": "application/json",
      "X-Shopify-Access-Token": accessToken
    },
    proxyReqPathResolver() {
      return `https://${shop}${GRAPHQL_PATH}`;
    }
  })(ctx);
};
