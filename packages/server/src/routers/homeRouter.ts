import * as Router from "koa-router";
// import * as Koa from "koa";

const homeRouter = new Router();

homeRouter.get("/install", async (ctx: any) => {
  ctx.state = {
    session: ctx.session,
    title: "app"
  };

  if (ctx.session && ctx.session.accessToken) {
    console.log(ctx.session);
    ctx.redirect("/");
  }
  await ctx.render("index.html");
});

export { homeRouter };
