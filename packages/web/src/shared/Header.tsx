import React from "react";
import { Query } from "react-apollo";
import { MeQuery } from "../schemaTypes";
import { meQuery } from "../graphql/queries/me";
import { renderNavDefault } from "./renderNavDefault";
import { renderLoginNav } from "./renderLoginNav";
import { renderAccountNav } from "./renderAccountNav";

export class Header extends React.PureComponent {
  render() {
    return (
      <div
        style={{
          height: 50,
          width: "100%",
          backgroundColor: "rgb(255, 254, 252)",
          display: "flex",
          justifyContent: "space-around",
          padding: 10,
          alignItems: "center"
        }}
      >
        {renderNavDefault}
        <Query<MeQuery> query={meQuery}>
          {({ data, loading, error }) => {
            if (loading) {
              return "loading";
            }

            if (!data || !data.me) {
              console.log(data);
              return renderLoginNav;
            }

            if (error) {
              return error.message;
            }

            // user is logged in

            return renderAccountNav;
          }}
        </Query>
      </div>
    );
  }
}
