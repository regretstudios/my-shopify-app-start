import React from "react";
import { Link } from "react-router-dom";
import { HeaderButton } from "../ui/HeaderButton";
export const renderNavDefault = (
  <Link to="/">
    <HeaderButton style={{ fontSize: 24 }}>Login Demo</HeaderButton>
  </Link>
);
