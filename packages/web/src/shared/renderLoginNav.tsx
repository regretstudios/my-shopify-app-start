import React from "react";
import { Link } from "react-router-dom";
import { HeaderButton } from "../ui/HeaderButton";
export const renderLoginNav = (
  <div>
    <Link to="/login">
      <HeaderButton>login</HeaderButton>
    </Link>
    <Link to="/register">
      <HeaderButton>register</HeaderButton>
    </Link>
  </div>
);
