import React from "react";
import ReactDOM from "react-dom";
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "react-apollo";

import { register as registerServiceWorker } from "./serviceWorker";
import { Routes } from "./Routes";
import { GlobalStyle } from "./ui/GlobalStyle";

const client = new ApolloClient({
  uri: "/api/graphql",
  credentials: "include"
});

ReactDOM.render(
  <ApolloProvider client={client}>
    <GlobalStyle />
    <Routes />
  </ApolloProvider>,
  document.getElementById("root") as HTMLElement
);
registerServiceWorker();
