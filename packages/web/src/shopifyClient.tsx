import ApolloClient from "apollo-boost";

export const shopifyClient = new ApolloClient({
  uri: "/api/test/graphql",
  fetchOptions: {
    credentials: "include"
  }

  // credentials: "include"
});
