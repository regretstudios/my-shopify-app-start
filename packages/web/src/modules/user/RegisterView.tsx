import React from "react";
import { Mutation } from "react-apollo";
import { gql } from "apollo-boost";
import { RouteComponentProps } from "react-router-dom";
import { isEmpty } from "ramda";
import { user as userValidate } from "@key46/validate";

import { RegisterMutationVariables, RegisterMutation } from "../../schemaTypes";
import { Form } from "./Form";

const registerMutation = gql`
  mutation RegisterMutation($email: String!, $password: String!) {
    register(email: $email, password: $password)
  }
`;

export class RegisterView extends React.PureComponent<RouteComponentProps<{}>> {
  render() {
    return (
      <Mutation<RegisterMutation, RegisterMutationVariables>
        mutation={registerMutation}
      >
        {mutate => {
          return (
            <>
              <Form
                buttonText="register"
                onSubmit={async data => {
                  const errors = await userValidate.validate(data);

                  if (isEmpty(errors)) {
                    throw new Error(JSON.stringify(errors));
                  }

                  const abc = await mutate({
                    variables: data
                  }).catch(error => {
                    alert(error.message);
                    throw error.message;
                  });
                  console.log(abc);
                  this.props.history.push("/login");
                }}
              />
            </>
          );
        }}
      </Mutation>
    );
  }
}
