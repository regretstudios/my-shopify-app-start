import React from "react";
import { Query } from "react-apollo";

import { MeQuery } from "../../../schemaTypes";
import { meQuery } from "../../../graphql/queries/me";

import { renderAccountUi } from "./renderAccountUi";
import { renderUiDataUndefind } from "./renderUiDataUndefind";
import { redirectToLogin } from "./redirectToLogin";

export class Account extends React.PureComponent {
  render() {
    return (
      <>
        <Query<MeQuery> query={meQuery}>
          {({ data, loading }) => {
            if (loading) {
              return null;
            }

            if (!data) {
              return renderUiDataUndefind;
            }

            if (!data.me) {
              return redirectToLogin;
            }

            if (data.me.type === "free-trial") {
              return <p>free-trial</p>;
            }

            // if (data.me.type === 'paid')
            return renderAccountUi(data);
          }}
        </Query>
      </>
    );
  }
}
