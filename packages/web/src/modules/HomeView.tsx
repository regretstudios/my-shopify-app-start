import React from "react";
import { gql } from "apollo-boost";
import { Query } from "react-apollo";

import { shopifyClient } from "../shopifyClient";
import { MeQuery } from "../schemaTypes";
import { meQuery } from "../graphql/queries/me";

const shopQuery = gql`
  query MeQuery {
    shop {
      name
      primaryDomain {
        url
        host
      }
    }
  }
`;

function HomeView({}) {
  return (
    <>
      <Query query={shopQuery} client={shopifyClient}>
        {({ data, loading }) => {
          if (loading) {
            return null;
          }

          if (!data) {
            return null;
          }
          return (
            <div>
              Shopify info:
              <div>
                <pre>{JSON.stringify(data, null, 2)}</pre>
              </div>
            </div>
          );
        }}
      </Query>
      <Query<MeQuery> query={meQuery}>
        {({ data, loading }) => {
          if (loading) {
            return null;
          }

          if (!data) {
            return null;
          }

          if (!data.me) {
            return null;
          }

          // if (data.me.type === "free-trial") {
          //   return <p>free-trial</p>;
          // }

          // if (data.me.type === 'paid')
          return (
            <div>
              Login:
              <div>
                <pre>{JSON.stringify(data, null, 2)}</pre>
              </div>
            </div>
          );
        }}
      </Query>
    </>
  );
}

export { HomeView };
