import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import { LoginView } from "./modules/user/LoginView";
import { RegisterView } from "./modules/user/RegisterView";
import { Account } from "./modules/account/Account/Account";
import { HomeView } from "./modules/HomeView";
import { Header } from "./shared/Header";

export const Routes = () => (
  <BrowserRouter>
    <Switch>
      <Route
        path="/"
        render={() => (
          <React.Fragment>
            <Header />
            <div>
              <Route path="/login" component={LoginView} />
              <Route path="/register" component={RegisterView} />
              <Route path="/account" component={Account} />
              <Route exact={true} path="/" render={() => <HomeView />} />
            </div>
          </React.Fragment>
        )}
      />
    </Switch>
  </BrowserRouter>
);
